#include <QGuiApplication>
#include <QKeyEvent>
#include <QOpenGLFunctions>
#include <QOpenGLWindow>

class HelloWindow : public QOpenGLWindow {
    Q_OBJECT

protected:
    void resizeGL(int width, int height) override
    {
        context()->functions()->glViewport(0, 0, width, height);
    }

    void paintGL() override
    {
        if (!isExposed())
            return;

        auto f = context()->functions();
        f->glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        f->glClear(GL_COLOR_BUFFER_BIT);
    }

    void keyPressEvent(QKeyEvent* event) override
    {
        if (event->key() == Qt::Key_Escape)
            close();
        QOpenGLWindow::keyPressEvent(event);
    }
};

int main(int argc, char** argv)
{
    // As per the docs, set format before QGuiApplication is created.
    QSurfaceFormat format;
    format.setMajorVersion(3);
    format.setMinorVersion(3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);

    QGuiApplication application(argc, argv);

    HelloWindow window;
    window.resize(800, 600);
    window.setTitle("Hello Window");
    window.show();

    return application.exec();
}


#include "01-hello-window.moc"
